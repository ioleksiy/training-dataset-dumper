from pathlib import Path


def main():
    """Loop over all block modules and add them to the api docs"""
    blocks_docs = Path('docs/ca_blocks.md')
    BASE = ":::FTagDumper.python.blocks."

    with blocks_docs.open('a') as f:
        blocks = Path('FTagDumper/python/blocks').glob('*.py')
        for block in sorted(blocks):
            if block.stem in {"BaseBlock", "__init__"}:
                continue
            s = f"{BASE}{block.stem}.{block.stem}\n"
            f.write(s)
    return

if __name__ == '__main__':
    main()