#ifndef HTRUTH_DECORATOR_ALG_HH
#define HTRUTH_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "xAODTracking/TrackParticleContainer.h"



/*
   This is a copy of 
   DerivationFrameworkMCTruth/​TruthClassificationDecorator.h
   to avoid requiring full Athena to access the 
   DerivationFrameworkMCTruth package. 
   
   A cleaner workaround would be to move the 
   ​TruthClassificationDecorator class to the 
   MCTruthClassifier package.
*/

class HitTruthDecoratorAlg :  public AthReentrantAlgorithm { 
public:
  
  /** Constructors */
  HitTruthDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);
  
  /** Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&) const override ;


private:


  SG::ReadHandleKey<xAOD::TruthParticleContainer>   m_Truths{this,"TruthParticles", "TruthParticles", "Truth Particles"};

  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetPixelCluster{this,"JetAssociatedPixelClusters", "JetAssociatedPixelClusters", "PixelClusters"};
  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetSCTCluster{this,"JetAssociatedSCTClusters", "JetAssociatedSCTClusters", "SCTClusters"};

  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterEnergyDep_B { this, "JetAssociatedPixelClustersEnergyDepB", m_JetPixelCluster, "EnergyDepB"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterEnergyDep_C { this, "JetAssociatedPixelClustersEnergyDepC", m_JetPixelCluster, "EnergyDepC"};  
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterTruthBarcode { this, "JetAssociatedPixelClustersTruthBarcode", m_JetPixelCluster, "TruthBarcode"};

  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterEnergyDep_B { this, "JetAssociatedSCTClustersEnergyDepB", m_JetSCTCluster, "EnergyDepB"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterEnergyDep_C { this, "JetAssociatedSCTClustersEnergyDepC", m_JetSCTCluster, "EnergyDepC"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterTruthBarcode { this, "JetAssociatedSCTClustersTruthBarcode", m_JetSCTCluster, "TruthBarcode"};


  template <typename T>
  using Acc = SG::AuxElement::ConstAccessor<T>;
  Acc<std::vector<int>> m_Barcode;
  Acc<std::vector<float>> m_Energy;


};


#endif