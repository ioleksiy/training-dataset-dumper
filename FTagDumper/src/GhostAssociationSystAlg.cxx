/************************************************************
 * @file GhostAssociationSystAlg.cxx
 * @brief Make a new track collection with ghost tracks if there is systematics applied
 * 
 * @author Marcus Vinicius Rodrigues (marcus.rodrigues@cern.ch)
 * **********************************************************/

#include "GhostAssociationSystAlg.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODPFlow/FlowElement.h"
#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"


GhostAssociationSystAlg::GhostAssociationSystAlg(
  const std::string& name, ISvcLocator* loc )
  : AthReentrantAlgorithm(name, loc), getCorrectedLinks()
  {
    declareProperty("GetCorrectedLinks", getCorrectedLinks);
  }

StatusCode GhostAssociationSystAlg::initialize() {
  ATH_MSG_DEBUG( "Inizializing " << name() << "... " );

  ATH_CHECK(m_tracksIn.initialize());
  ATH_CHECK(m_tracksOut.initialize());
  ATH_CHECK(m_jet_collection.initialize());

  return StatusCode::SUCCESS;
}

StatusCode GhostAssociationSystAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG( "Executing " << name() << "... " );

  // =========================================================
  
  SG::ReadHandle jets(m_jet_collection, ctx);
  SG::ReadDecorHandle<IPC, IPLV> tracksIn(m_tracksIn, ctx);
  SG::WriteDecorHandle<IPC, IPLV> tracksOut(m_tracksOut, ctx);
  static const SG::AuxElement::ConstAccessor<IPL> correctedTrackLinkAcc("correctedTrackLink");
  
  for(const auto& jet: *jets){  
    if(getCorrectedLinks){ // In case there is syst and we have to get the updated links
      std::vector<ElementLink<xAOD::IParticleContainer>> tracks = tracksIn(*jet);
      IPLV links;
      for(const auto& track: tracks){
        IPL link = correctedTrackLinkAcc(**track);
        links.push_back(link);
      }
      tracksOut(*jet) = links;
    }
    else{
      tracksOut(*jet) = tracksIn(*jet);
    }
  }
  
  return StatusCode::SUCCESS;
}