Jet Writers
===========

This packages has some code to write jet information to HDF5.


Constituent Writers
-------------------

There are a few generic classes to write "constituents", i.e. any
vector of particles associated to a jet. Each of these has a `Config`
class that it can be initialized with.

- `JetConstituentWriter`: has a `write(jet, constituents)` method.
- `JetLinkWriter`: just has a `write(jet)` method, we assume you have
  already associated something.


Miscellaneous Classes
---------------------

Some classes don't fall in another category.

- `AssociationConfig`: Base templates to retrieve particles associated
  with constituents. This is used if, for example, you want to
  retrieve tracks associated to particle flow objects.
- `JetElement`: Internal class, holds EDM objects before they are
  translated to raw data types in the HDF5 writer.

